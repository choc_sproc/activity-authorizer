'use strict';

var Code = require('code');
var Hapi = require('hapi');
var Lab = require('lab');
var Hapi = require('hapi');
var Boom = require('boom');

// Test shortcuts
var lab = exports.lab = Lab.script();
var describe = lab.describe;
var it = lab.it;
var expect = Code.expect;

var plugin = require('../');

function setup(defaultAccess) {
	var server = new Hapi.Server();
	server.connection();
	server.register({
		register: plugin,
		options: {
			activities: {
				testActivity: ['admin'],
				fnActivity: function(request, reply) {
					if (request.auth.credentials.role === 'admin') {
						reply.continue();
					} else {
						reply(Boom.forbidden('Unauthorized activity via function.'));
					}
				},
				allActivity: 'all'
			},
			defaultAction: defaultAccess
		}
	}, function(err) {
		expect(err).to.not.exist();
	});
	return server;
}

describe('setup', function() {

	it('fails when no activites are passed in on the options for the registration function', function(done) {

		var server = new Hapi.Server();
		server.connection();
		server.register(plugin, function(err) {

			expect(err).to.be.an.instanceof(Error);
			expect(err.message).to.equal('No activities declared, nothing will be filtered based on activities passed by routes.');
			done();

		});

	});

	it('fails when activites are not arrays or functions or a string set to "all"', function(done) {

		var server = new Hapi.Server();
		server.connection();
		server.register({
			register: plugin,
			options: {
				activities: {
					testActivity: 'role'
				}
			}
		}, function(err) {

			expect(err).to.be.an.instanceof(Error);
			expect(err.message).to.equal('Activities must be an array of allowed roles, a function or the string "all".');
			done();

		});

	});

	it('fails when default action is invalid value', function(done) {

		var server = new Hapi.Server();
		server.connection();
		server.register({
			register: plugin,
			options: {
				activities: {
					testActivity: ['admin']
				},
				defaultAction: 'badAction'
			}
		}, function(err) {

			expect(err).to.be.an.instanceof(Error);
			expect(err.message).to.equal('defaultAction must be set to "allow" or "deny" (default is allow).');
			done();

		});

	});

	it('successfully registers the plugin when an activity is set to "all"', function(done) {

		var server = new Hapi.Server();
		server.connection();
		server.register({
			register: plugin,
			options: {
				activities: {
					testActivity: 'all'
				},
				defaultAction: 'deny'
			}
		}, function(err) {

			expect(err).to.not.exist();
			done();

		});

	});

	it('successfully registers the plugin', function(done) {

		var server = new Hapi.Server();
		server.connection();
		server.register({
			register: plugin,
			options: {
				activities: {
					testActivity: ['admin']
				},
				defaultAction: 'deny'
			}
		}, function(err) {

			expect(err).to.not.exist();
			done();

		});

	});

});

describe('authorization', function() {

	it('allows a user with no activity set on the route and defaultAction set to allow', function(done) {

		var server = new Hapi.Server();
		server.connection();
		server.register({
			register: plugin,
			options: {
				activities: {
					testActivity: ['admin']
				}
			}
		}, function(err) {
			expect(err).to.not.exist();
		});

		server.route({
			method: 'GET',
			path: '/',
			handler: function(request, reply) {
				reply('ok');
			}
		});

		var request = { method: 'GET', url: '/' };

		server.inject(request, function(res) {

			expect(res.statusCode).to.equal(200);
			done();

		});

	});

	it('denies a user with no activity set on the route and defaultAction set to deny', function(done) {

		var server = setup('deny');

		server.route({
			method: 'GET',
			path: '/',
			handler: function(request, reply) {
				reply('ok');
			}
		});

		var request = { method: 'GET', url: '/' };

		server.inject(request, function(res) {

			expect(res.statusCode).to.equal(403);
			done();

		});

	});

	it('denies a user with activity set on the route but no registered activity', function(done) {

		var server = setup('deny');

		server.route({
			method: 'GET',
			path: '/',
			handler: function(request, reply) {
				reply('ok');
			},
			config: {
				plugins: {
					'activity-authorizer': {}
				}
			}
		});

		var request = { method: 'GET', url: '/' };

		server.inject(request, function(res) {

			expect(res.statusCode).to.equal(403);
			done();

		});

	});

	it('denies a user with activity set on the route but no role on user', function(done) {

		var server = setup('deny');

		server.route({
			method: 'GET',
			path: '/',
			handler: function(request, reply) {
				reply('ok');
			},
			config: {
				plugins: {
					'activity-authorizer': {
						activity: 'testActivity'
					}
				}
			}
		});

		var request = { method: 'GET', url: '/',
			isAuthenticated: true,
			credentials: {}
		};

		server.inject(request, function(res) {

			expect(res.statusCode).to.equal(403);
			done();

		});

	});

	it('denies a user with activity set on the route but not authorized role', function(done) {

		var server = setup('deny');

		server.route({
			method: 'GET',
			path: '/',
			handler: function(request, reply) {
				reply('ok');
			},
			config: {
				plugins: {
					'activity-authorizer': {
						activity: 'testActivity'
					}
				}
			}
		});

		var request = { method: 'GET', url: '/',
			isAuthenticated: true,
			credentials: {
				role: 'lsr'
			}
		};

		server.inject(request, function(res) {

			expect(res.statusCode).to.equal(403);
			done();

		});

	});

	it('allows a user with activity set on the route', function(done) {

		var server = setup('deny');

		server.route({
			method: 'GET',
			path: '/',
			handler: function(request, reply) {
				reply('ok');
			},
			config: {
				plugins: {
					'activity-authorizer': {
						activity: 'testActivity'
					}
				}
			}
		});

		var request = { method: 'GET', url: '/', credentials: {
			role: 'admin'
		} };

		server.inject(request, function(res) {

			expect(res.statusCode).to.equal(200);
			done();

		});

	});

	it('allows a user with function activity set on the route', function(done) {

		var server = setup('deny');

		server.route({
			method: 'GET',
			path: '/',
			handler: function(request, reply) {
				reply('ok');
			},
			config: {
				plugins: {
					'activity-authorizer': {
						activity: 'fnActivity'
					}
				}
			}
		});

		var request = { method: 'GET', url: '/', credentials: {
			role: 'admin'
		} };

		server.inject(request, function(res) {

			expect(res.statusCode).to.equal(200);
			done();

		});

	});

	it('denies a user with function activity set on the route', function(done) {

		var server = setup('deny');

		server.route({
			method: 'GET',
			path: '/',
			handler: function(request, reply) {
				reply('ok');
			},
			config: {
				plugins: {
					'activity-authorizer': {
						activity: 'testActivity'
					}
				}
			}
		});

		var request = { method: 'GET', url: '/', credentials: {
			role: 'lsr'
		} };

		server.inject(request, function(res) {

			expect(res.statusCode).to.equal(403);
			done();

		});

	});

	it('allows a user with activity set on the route with "all" as roles allowed', function(done) {

		var server = setup('deny');

		server.route({
			method: 'GET',
			path: '/',
			handler: function(request, reply) {
				reply('ok');
			},
			config: {
				plugins: {
					'activity-authorizer': {
						activity: 'allActivity'
					}
				}
			}
		});

		var request = { method: 'GET', url: '/', credentials: {
			role: 'lsr'
		} };

		server.inject(request, function(res) {

			expect(res.statusCode).to.equal(200);
			done();

		});

	});

	it('denies a user that is not logged in with activity set on route and roles set as "all"', function(done) {

		var server = setup('deny');

		server.route({
			method: 'GET',
			path: '/',
			handler: function(request, reply) {
				reply('ok');
			},
			config: {
				plugins: {
					'activity-authorizer': {
						activity: 'allActivity'
					}
				}
			}
		});

		var request = { method: 'GET', url: '/'};

		server.inject(request, function(res) {

			expect(res.statusCode).to.equal(403);
			done();

		});

	});

	it('denies a user when credentials are set on request, but they are empty', function(done) {

		var server = setup('deny');

		server.route({
			method: 'GET',
			path: '/',
			handler: function(request, reply) {
				reply('ok');
			},
			config: {
				plugins: {
					'activity-authorizer': {
						activity: 'allActivity'
					}
				}
			}
		});

		var request = { method: 'GET', url: '/', credentials: {} };


		server.inject(request, function(res) {

			expect(res.statusCode).to.equal(403);
			done();

		});

	});

	it('denies a user when no role is set', function(done) {

		var server = setup('deny');

		server.route({
			method: 'GET',
			path: '/',
			handler: function(request, reply) {
				reply('ok');
			},
			config: {
				plugins: {
					'activity-authorizer': {
						activity: 'allActivity'
					}
				}
			}
		});

		var request = { method: 'GET', url: '/', credentials: { } };


		server.inject(request, function(res) {

			expect(res.statusCode).to.equal(403);
			done();

		});

	});

});
