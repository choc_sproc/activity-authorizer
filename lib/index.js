'use strict';

var Boom = require('boom');
var _ = require('lodash');

var activities = {};

exports.register = function(server, options, next) {

	if ( ! _.has(options, 'activities')) {
		return next(new Error('No activities declared, nothing will be filtered based on activities passed by routes.'));
	} else {
		// loop through and confirm the activities are all arrays
		for (var key in options.activities) {
			if ( ! _.isArray(options.activities[key]) && ! _.isFunction(options.activities[key]) && options.activities[key] !== 'all') {
				return next(new Error('Activities must be an array of allowed roles, a function or the string "all".'));
			}
		}
		activities = _.cloneDeep(options.activities);
	}

	if ( ! _.has(options, 'defaultAction')) {
		options.defaultAction = 'allow';
	} else if (['allow','deny'].indexOf(options.defaultAction.toLowerCase()) === -1) {
		return next(new Error('defaultAction must be set to "allow" or "deny" (default is allow).'));
	} else {
		options.defaultAction = options.defaultAction.toLowerCase();
	}

	server.ext('onPreHandler', function(request, reply) {

		// check first to see if there is config options for this plugin on the route
		// and if there is an activity defined and if there is an activity that was passed
		// in when the plugin was setup.  If all of that is true then go ahead and see
		// about authorizing the user, otherwise determine what action to take. Default
		// is to allow it to pass through.
		if (request.route.settings.plugins['activity-authorizer'] &&
			request.route.settings.plugins['activity-authorizer'].activity !== undefined &&
			_.has(activities, request.route.settings.plugins['activity-authorizer'].activity)) {

			var activity = activities[request.route.settings.plugins['activity-authorizer'].activity];

			if (_.isFunction(activity)) {

				activity(request, reply);

			} else {

				// based on the user's role, check to see what their role is and see if this
				// activity is contained in that role.
				if (request.auth.credentials && _.has(request.auth.credentials, 'role')) {

					if (activity === 'all') {

						// as long as they have a role assigned this is authorized
						reply.continue();

					} else if (activity.indexOf(request.auth.credentials.role) > -1) {

						// found their role, they are authorized
						reply.continue();

					} else {
						reply(Boom.forbidden('Not authorized to perform this action'));
					}
				} else {
					reply(Boom.forbidden('Not authorized, to perform this action.'));
				}
			}

		} else {
			if (options.defaultAction === 'deny') {
				reply(Boom.forbidden('Not authorized to perform this action.'));
			} else {
				reply.continue();
			}
		}

	});

	next();
};

exports.register.attributes = {
	pkg: require('../package.json')
};
